import express from 'express';
import dotenv from 'dotenv';

dotenv.config();
const router = express.Router();
/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Backend autoFill Form by voice recognition' });
});

export default router;
