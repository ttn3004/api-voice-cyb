/* eslint-env node */
import index from './routes/index';
import livereload from 'livereload';
import connectLivereload from 'connect-livereload';
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var app = express();
const env = process.env.NODE_ENV || 'development';
/* Setting environment variable for bash shell
export NODE_ENV=production
OR
 Defined while starting the application
NODE_ENV=production node app.js

livereload : opens a high port and notifies the browser of any changed file
connect-livereload :monkey patches every served HTML page with a JavaScript snippet that connects to this high port
nodemon :restarts the server on any changed backend file
*/
if (env == 'development') {
  console.log('DEV mode');
  const livereloadServer = livereload.createServer();
  livereloadServer.watch(path.join(__dirname, './../public'));
  // ping browser on Express boot, once browser has reconnected and handshaken
  livereloadServer.server.once('connection', () => {
    setTimeout(() => {
      livereloadServer.refresh('/');
    }, 100);
  });
  app.use(connectLivereload());
}
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, './../public')));

app.use('/', index);
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

const WebSocketServer = require('ws').Server;
const wsserver = new WebSocketServer({ port: 4201 });

wsserver.on('connection', function connection(socket) {
  socket.on('message', function incoming(message) {
    console.log('received: %s', message);
    /**
    
    
    Ici on va procceder la recognition vocal par l execution d'un script CPP.
    Une fonction de conversion de format sera implémentee selon les besoins de Teddy avant d execution
    La reponse retournee est renvoye au client
    
    
     */
    socket.send(JSON.stringify({ name: 'yes received', data: message }));
  });

  socket.send('Hey! Welcome to my websocket challenge!'); // notifier au client que la connexion est etablie
});
// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
