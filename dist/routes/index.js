'use strict';Object.defineProperty(exports, "__esModule", { value: true });var _express = require('express');var _express2 = _interopRequireDefault(_express);
var _dotenv = require('dotenv');var _dotenv2 = _interopRequireDefault(_dotenv);function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

_dotenv2.default.config();
var router = _express2.default.Router();
/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Backend autoFill Form by voice recognition' });
});exports.default =

router;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NlcnZlci9yb3V0ZXMvaW5kZXguanMiXSwibmFtZXMiOlsiZG90ZW52IiwiY29uZmlnIiwicm91dGVyIiwiZXhwcmVzcyIsIlJvdXRlciIsImdldCIsInJlcSIsInJlcyIsIm5leHQiLCJyZW5kZXIiLCJ0aXRsZSJdLCJtYXBwaW5ncyI6IjJFQUFBLGtDO0FBQ0EsZ0M7O0FBRUFBLGlCQUFPQyxNQUFQO0FBQ0EsSUFBTUMsU0FBU0Msa0JBQVFDLE1BQVIsRUFBZjtBQUNBO0FBQ0FGLE9BQU9HLEdBQVAsQ0FBVyxHQUFYLEVBQWdCLFVBQVVDLEdBQVYsRUFBZUMsR0FBZixFQUFvQkMsSUFBcEIsRUFBMEI7QUFDeENELE1BQUlFLE1BQUosQ0FBVyxPQUFYLEVBQW9CLEVBQUVDLE9BQU8sNENBQVQsRUFBcEI7QUFDRCxDQUZELEU7O0FBSWVSLE0iLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgZXhwcmVzcyBmcm9tICdleHByZXNzJztcbmltcG9ydCBkb3RlbnYgZnJvbSAnZG90ZW52JztcblxuZG90ZW52LmNvbmZpZygpO1xuY29uc3Qgcm91dGVyID0gZXhwcmVzcy5Sb3V0ZXIoKTtcbi8qIEdFVCBob21lIHBhZ2UuICovXG5yb3V0ZXIuZ2V0KCcvJywgZnVuY3Rpb24gKHJlcSwgcmVzLCBuZXh0KSB7XG4gIHJlcy5yZW5kZXIoJ2luZGV4JywgeyB0aXRsZTogJ0JhY2tlbmQgYXV0b0ZpbGwgRm9ybSBieSB2b2ljZSByZWNvZ25pdGlvbicgfSk7XG59KTtcblxuZXhwb3J0IGRlZmF1bHQgcm91dGVyO1xuIl19