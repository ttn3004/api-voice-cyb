'use strict';
var _index = require('./routes/index');var _index2 = _interopRequireDefault(_index);
var _livereload = require('livereload');var _livereload2 = _interopRequireDefault(_livereload);
var _connectLivereload = require('connect-livereload');var _connectLivereload2 = _interopRequireDefault(_connectLivereload);function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}
var createError = require('http-errors'); /* eslint-env node */
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var app = express();
var env = process.env.NODE_ENV || 'development';
/* Setting environment variable for bash shell
                                                 export NODE_ENV=production
                                                 OR
                                                  Defined while starting the application
                                                 NODE_ENV=production node app.js
                                                 
                                                 livereload : opens a high port and notifies the browser of any changed file
                                                 connect-livereload :monkey patches every served HTML page with a JavaScript snippet that connects to this high port
                                                 nodemon :restarts the server on any changed backend file
                                                 */
if (env == 'development') {
  console.log('DEV mode');
  var livereloadServer = _livereload2.default.createServer();
  livereloadServer.watch(path.join(__dirname, './../public'));
  // ping browser on Express boot, once browser has reconnected and handshaken
  livereloadServer.server.once('connection', function () {
    setTimeout(function () {
      livereloadServer.refresh('/');
    }, 100);
  });
  app.use((0, _connectLivereload2.default)());
}
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, './../public')));

app.use('/', _index2.default);
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

var WebSocketServer = require('ws').Server;
var wsserver = new WebSocketServer({ port: 4201 });

wsserver.on('connection', function connection(socket) {
  socket.on('message', function incoming(message) {
    console.log('received: %s', message);
    /**
                                          
                                          
                                          Ici on va procceder la recognition vocal par l execution d'un script CPP.
                                          Une fonction de conversion de format sera implémentee selon les besoins de Teddy avant d execution
                                          La reponse retournee est renvoye au client
                                          
                                          
                                           */
    socket.send(JSON.stringify({ name: 'yes received', data: message }));
  });

  socket.send('Hey! Welcome to my websocket challenge!'); // notifier au client que la connexion est etablie
});
// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NlcnZlci9hcHAuanMiXSwibmFtZXMiOlsiY3JlYXRlRXJyb3IiLCJyZXF1aXJlIiwiZXhwcmVzcyIsInBhdGgiLCJjb29raWVQYXJzZXIiLCJsb2dnZXIiLCJhcHAiLCJlbnYiLCJwcm9jZXNzIiwiTk9ERV9FTlYiLCJjb25zb2xlIiwibG9nIiwibGl2ZXJlbG9hZFNlcnZlciIsImxpdmVyZWxvYWQiLCJjcmVhdGVTZXJ2ZXIiLCJ3YXRjaCIsImpvaW4iLCJfX2Rpcm5hbWUiLCJzZXJ2ZXIiLCJvbmNlIiwic2V0VGltZW91dCIsInJlZnJlc2giLCJ1c2UiLCJzZXQiLCJqc29uIiwidXJsZW5jb2RlZCIsImV4dGVuZGVkIiwic3RhdGljIiwiaW5kZXgiLCJyZXEiLCJyZXMiLCJuZXh0IiwiV2ViU29ja2V0U2VydmVyIiwiU2VydmVyIiwid3NzZXJ2ZXIiLCJwb3J0Iiwib24iLCJjb25uZWN0aW9uIiwic29ja2V0IiwiaW5jb21pbmciLCJtZXNzYWdlIiwic2VuZCIsIkpTT04iLCJzdHJpbmdpZnkiLCJuYW1lIiwiZGF0YSIsImVyciIsImxvY2FscyIsImVycm9yIiwiZ2V0Iiwic3RhdHVzIiwicmVuZGVyIiwibW9kdWxlIiwiZXhwb3J0cyJdLCJtYXBwaW5ncyI6IjtBQUNBLHVDO0FBQ0Esd0M7QUFDQSx1RDtBQUNBLElBQUlBLGNBQWNDLFFBQVEsYUFBUixDQUFsQixDLENBSkE7QUFLQSxJQUFJQyxVQUFVRCxRQUFRLFNBQVIsQ0FBZDtBQUNBLElBQUlFLE9BQU9GLFFBQVEsTUFBUixDQUFYO0FBQ0EsSUFBSUcsZUFBZUgsUUFBUSxlQUFSLENBQW5CO0FBQ0EsSUFBSUksU0FBU0osUUFBUSxRQUFSLENBQWI7QUFDQSxJQUFJSyxNQUFNSixTQUFWO0FBQ0EsSUFBTUssTUFBTUMsUUFBUUQsR0FBUixDQUFZRSxRQUFaLElBQXdCLGFBQXBDO0FBQ0E7Ozs7Ozs7Ozs7QUFVQSxJQUFJRixPQUFPLGFBQVgsRUFBMEI7QUFDeEJHLFVBQVFDLEdBQVIsQ0FBWSxVQUFaO0FBQ0EsTUFBTUMsbUJBQW1CQyxxQkFBV0MsWUFBWCxFQUF6QjtBQUNBRixtQkFBaUJHLEtBQWpCLENBQXVCWixLQUFLYSxJQUFMLENBQVVDLFNBQVYsRUFBcUIsYUFBckIsQ0FBdkI7QUFDQTtBQUNBTCxtQkFBaUJNLE1BQWpCLENBQXdCQyxJQUF4QixDQUE2QixZQUE3QixFQUEyQyxZQUFNO0FBQy9DQyxlQUFXLFlBQU07QUFDZlIsdUJBQWlCUyxPQUFqQixDQUF5QixHQUF6QjtBQUNELEtBRkQsRUFFRyxHQUZIO0FBR0QsR0FKRDtBQUtBZixNQUFJZ0IsR0FBSixDQUFRLGtDQUFSO0FBQ0Q7QUFDRDtBQUNBaEIsSUFBSWlCLEdBQUosQ0FBUSxPQUFSLEVBQWlCcEIsS0FBS2EsSUFBTCxDQUFVQyxTQUFWLEVBQXFCLE9BQXJCLENBQWpCO0FBQ0FYLElBQUlpQixHQUFKLENBQVEsYUFBUixFQUF1QixNQUF2Qjs7QUFFQWpCLElBQUlnQixHQUFKLENBQVFqQixPQUFPLEtBQVAsQ0FBUjtBQUNBQyxJQUFJZ0IsR0FBSixDQUFRcEIsUUFBUXNCLElBQVIsRUFBUjtBQUNBbEIsSUFBSWdCLEdBQUosQ0FBUXBCLFFBQVF1QixVQUFSLENBQW1CLEVBQUVDLFVBQVUsS0FBWixFQUFuQixDQUFSO0FBQ0FwQixJQUFJZ0IsR0FBSixDQUFRbEIsY0FBUjtBQUNBRSxJQUFJZ0IsR0FBSixDQUFRcEIsUUFBUXlCLE1BQVIsQ0FBZXhCLEtBQUthLElBQUwsQ0FBVUMsU0FBVixFQUFxQixhQUFyQixDQUFmLENBQVI7O0FBRUFYLElBQUlnQixHQUFKLENBQVEsR0FBUixFQUFhTSxlQUFiO0FBQ0E7QUFDQXRCLElBQUlnQixHQUFKLENBQVEsVUFBVU8sR0FBVixFQUFlQyxHQUFmLEVBQW9CQyxJQUFwQixFQUEwQjtBQUNoQ0EsT0FBSy9CLFlBQVksR0FBWixDQUFMO0FBQ0QsQ0FGRDs7QUFJQSxJQUFNZ0Msa0JBQWtCL0IsUUFBUSxJQUFSLEVBQWNnQyxNQUF0QztBQUNBLElBQU1DLFdBQVcsSUFBSUYsZUFBSixDQUFvQixFQUFFRyxNQUFNLElBQVIsRUFBcEIsQ0FBakI7O0FBRUFELFNBQVNFLEVBQVQsQ0FBWSxZQUFaLEVBQTBCLFNBQVNDLFVBQVQsQ0FBb0JDLE1BQXBCLEVBQTRCO0FBQ3BEQSxTQUFPRixFQUFQLENBQVUsU0FBVixFQUFxQixTQUFTRyxRQUFULENBQWtCQyxPQUFsQixFQUEyQjtBQUM5QzlCLFlBQVFDLEdBQVIsQ0FBWSxjQUFaLEVBQTRCNkIsT0FBNUI7QUFDQTs7Ozs7Ozs7O0FBU0FGLFdBQU9HLElBQVAsQ0FBWUMsS0FBS0MsU0FBTCxDQUFlLEVBQUVDLE1BQU0sY0FBUixFQUF3QkMsTUFBTUwsT0FBOUIsRUFBZixDQUFaO0FBQ0QsR0FaRDs7QUFjQUYsU0FBT0csSUFBUCxDQUFZLHlDQUFaLEVBZm9ELENBZUk7QUFDekQsQ0FoQkQ7QUFpQkE7QUFDQW5DLElBQUlnQixHQUFKLENBQVEsVUFBVXdCLEdBQVYsRUFBZWpCLEdBQWYsRUFBb0JDLEdBQXBCLEVBQXlCQyxJQUF6QixFQUErQjtBQUNyQztBQUNBRCxNQUFJaUIsTUFBSixDQUFXUCxPQUFYLEdBQXFCTSxJQUFJTixPQUF6QjtBQUNBVixNQUFJaUIsTUFBSixDQUFXQyxLQUFYLEdBQW1CbkIsSUFBSXZCLEdBQUosQ0FBUTJDLEdBQVIsQ0FBWSxLQUFaLE1BQXVCLGFBQXZCLEdBQXVDSCxHQUF2QyxHQUE2QyxFQUFoRTtBQUNBO0FBQ0FoQixNQUFJb0IsTUFBSixDQUFXSixJQUFJSSxNQUFKLElBQWMsR0FBekI7QUFDQXBCLE1BQUlxQixNQUFKLENBQVcsT0FBWDtBQUNELENBUEQ7O0FBU0FDLE9BQU9DLE9BQVAsR0FBaUIvQyxHQUFqQiIsImZpbGUiOiJhcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBlc2xpbnQtZW52IG5vZGUgKi9cbmltcG9ydCBpbmRleCBmcm9tICcuL3JvdXRlcy9pbmRleCc7XG5pbXBvcnQgbGl2ZXJlbG9hZCBmcm9tICdsaXZlcmVsb2FkJztcbmltcG9ydCBjb25uZWN0TGl2ZXJlbG9hZCBmcm9tICdjb25uZWN0LWxpdmVyZWxvYWQnO1xudmFyIGNyZWF0ZUVycm9yID0gcmVxdWlyZSgnaHR0cC1lcnJvcnMnKTtcbnZhciBleHByZXNzID0gcmVxdWlyZSgnZXhwcmVzcycpO1xudmFyIHBhdGggPSByZXF1aXJlKCdwYXRoJyk7XG52YXIgY29va2llUGFyc2VyID0gcmVxdWlyZSgnY29va2llLXBhcnNlcicpO1xudmFyIGxvZ2dlciA9IHJlcXVpcmUoJ21vcmdhbicpO1xudmFyIGFwcCA9IGV4cHJlc3MoKTtcbmNvbnN0IGVudiA9IHByb2Nlc3MuZW52Lk5PREVfRU5WIHx8ICdkZXZlbG9wbWVudCc7XG4vKiBTZXR0aW5nIGVudmlyb25tZW50IHZhcmlhYmxlIGZvciBiYXNoIHNoZWxsXG5leHBvcnQgTk9ERV9FTlY9cHJvZHVjdGlvblxuT1JcbiBEZWZpbmVkIHdoaWxlIHN0YXJ0aW5nIHRoZSBhcHBsaWNhdGlvblxuTk9ERV9FTlY9cHJvZHVjdGlvbiBub2RlIGFwcC5qc1xuXG5saXZlcmVsb2FkIDogb3BlbnMgYSBoaWdoIHBvcnQgYW5kIG5vdGlmaWVzIHRoZSBicm93c2VyIG9mIGFueSBjaGFuZ2VkIGZpbGVcbmNvbm5lY3QtbGl2ZXJlbG9hZCA6bW9ua2V5IHBhdGNoZXMgZXZlcnkgc2VydmVkIEhUTUwgcGFnZSB3aXRoIGEgSmF2YVNjcmlwdCBzbmlwcGV0IHRoYXQgY29ubmVjdHMgdG8gdGhpcyBoaWdoIHBvcnRcbm5vZGVtb24gOnJlc3RhcnRzIHRoZSBzZXJ2ZXIgb24gYW55IGNoYW5nZWQgYmFja2VuZCBmaWxlXG4qL1xuaWYgKGVudiA9PSAnZGV2ZWxvcG1lbnQnKSB7XG4gIGNvbnNvbGUubG9nKCdERVYgbW9kZScpO1xuICBjb25zdCBsaXZlcmVsb2FkU2VydmVyID0gbGl2ZXJlbG9hZC5jcmVhdGVTZXJ2ZXIoKTtcbiAgbGl2ZXJlbG9hZFNlcnZlci53YXRjaChwYXRoLmpvaW4oX19kaXJuYW1lLCAnLi8uLi9wdWJsaWMnKSk7XG4gIC8vIHBpbmcgYnJvd3NlciBvbiBFeHByZXNzIGJvb3QsIG9uY2UgYnJvd3NlciBoYXMgcmVjb25uZWN0ZWQgYW5kIGhhbmRzaGFrZW5cbiAgbGl2ZXJlbG9hZFNlcnZlci5zZXJ2ZXIub25jZSgnY29ubmVjdGlvbicsICgpID0+IHtcbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgIGxpdmVyZWxvYWRTZXJ2ZXIucmVmcmVzaCgnLycpO1xuICAgIH0sIDEwMCk7XG4gIH0pO1xuICBhcHAudXNlKGNvbm5lY3RMaXZlcmVsb2FkKCkpO1xufVxuLy8gdmlldyBlbmdpbmUgc2V0dXBcbmFwcC5zZXQoJ3ZpZXdzJywgcGF0aC5qb2luKF9fZGlybmFtZSwgJ3ZpZXdzJykpO1xuYXBwLnNldCgndmlldyBlbmdpbmUnLCAnamFkZScpO1xuXG5hcHAudXNlKGxvZ2dlcignZGV2JykpO1xuYXBwLnVzZShleHByZXNzLmpzb24oKSk7XG5hcHAudXNlKGV4cHJlc3MudXJsZW5jb2RlZCh7IGV4dGVuZGVkOiBmYWxzZSB9KSk7XG5hcHAudXNlKGNvb2tpZVBhcnNlcigpKTtcbmFwcC51c2UoZXhwcmVzcy5zdGF0aWMocGF0aC5qb2luKF9fZGlybmFtZSwgJy4vLi4vcHVibGljJykpKTtcblxuYXBwLnVzZSgnLycsIGluZGV4KTtcbi8vIGNhdGNoIDQwNCBhbmQgZm9yd2FyZCB0byBlcnJvciBoYW5kbGVyXG5hcHAudXNlKGZ1bmN0aW9uIChyZXEsIHJlcywgbmV4dCkge1xuICBuZXh0KGNyZWF0ZUVycm9yKDQwNCkpO1xufSk7XG5cbmNvbnN0IFdlYlNvY2tldFNlcnZlciA9IHJlcXVpcmUoJ3dzJykuU2VydmVyO1xuY29uc3Qgd3NzZXJ2ZXIgPSBuZXcgV2ViU29ja2V0U2VydmVyKHsgcG9ydDogNDIwMSB9KTtcblxud3NzZXJ2ZXIub24oJ2Nvbm5lY3Rpb24nLCBmdW5jdGlvbiBjb25uZWN0aW9uKHNvY2tldCkge1xuICBzb2NrZXQub24oJ21lc3NhZ2UnLCBmdW5jdGlvbiBpbmNvbWluZyhtZXNzYWdlKSB7XG4gICAgY29uc29sZS5sb2coJ3JlY2VpdmVkOiAlcycsIG1lc3NhZ2UpO1xuICAgIC8qKlxuICAgIFxuICAgIFxuICAgIEljaSBvbiB2YSBwcm9jY2VkZXIgbGEgcmVjb2duaXRpb24gdm9jYWwgcGFyIGwgZXhlY3V0aW9uIGQndW4gc2NyaXB0IENQUC5cbiAgICBVbmUgZm9uY3Rpb24gZGUgY29udmVyc2lvbiBkZSBmb3JtYXQgc2VyYSBpbXBsw6ltZW50ZWUgc2Vsb24gbGVzIGJlc29pbnMgZGUgVGVkZHkgYXZhbnQgZCBleGVjdXRpb25cbiAgICBMYSByZXBvbnNlIHJldG91cm5lZSBlc3QgcmVudm95ZSBhdSBjbGllbnRcbiAgICBcbiAgICBcbiAgICAgKi9cbiAgICBzb2NrZXQuc2VuZChKU09OLnN0cmluZ2lmeSh7IG5hbWU6ICd5ZXMgcmVjZWl2ZWQnLCBkYXRhOiBtZXNzYWdlIH0pKTtcbiAgfSk7XG5cbiAgc29ja2V0LnNlbmQoJ0hleSEgV2VsY29tZSB0byBteSB3ZWJzb2NrZXQgY2hhbGxlbmdlIScpOyAvLyBub3RpZmllciBhdSBjbGllbnQgcXVlIGxhIGNvbm5leGlvbiBlc3QgZXRhYmxpZVxufSk7XG4vLyBlcnJvciBoYW5kbGVyXG5hcHAudXNlKGZ1bmN0aW9uIChlcnIsIHJlcSwgcmVzLCBuZXh0KSB7XG4gIC8vIHNldCBsb2NhbHMsIG9ubHkgcHJvdmlkaW5nIGVycm9yIGluIGRldmVsb3BtZW50XG4gIHJlcy5sb2NhbHMubWVzc2FnZSA9IGVyci5tZXNzYWdlO1xuICByZXMubG9jYWxzLmVycm9yID0gcmVxLmFwcC5nZXQoJ2VudicpID09PSAnZGV2ZWxvcG1lbnQnID8gZXJyIDoge307XG4gIC8vIHJlbmRlciB0aGUgZXJyb3IgcGFnZVxuICByZXMuc3RhdHVzKGVyci5zdGF0dXMgfHwgNTAwKTtcbiAgcmVzLnJlbmRlcignZXJyb3InKTtcbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGFwcDtcbiJdfQ==