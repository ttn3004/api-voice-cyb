#!/usr/bin/env node
'use strict';

/**
               * Module dependencies.
               */

var app = require('../app');
var debug = require('debug')('api-ws:server');
var http = require('http');

/**
                             * Get port from environment and store in Express.
                             */

var port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

/**
                        * Create HTTP server.
                        */

var server = http.createServer(app);

/**
                                      * Listen on provided port, on all network interfaces.
                                      */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
                                      * Normalize a port into a number, string, or false.
                                      */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
   * Event listener for HTTP server "error" event.
   */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string' ?
  'Pipe ' + port :
  'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;}

}

/**
   * Event listener for HTTP server "listening" event.
   */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string' ?
  'pipe ' + addr :
  'port ' + addr.port;
  debug('Listening on ' + bind);
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NlcnZlci9iaW4vd3d3LmpzIl0sIm5hbWVzIjpbImFwcCIsInJlcXVpcmUiLCJkZWJ1ZyIsImh0dHAiLCJwb3J0Iiwibm9ybWFsaXplUG9ydCIsInByb2Nlc3MiLCJlbnYiLCJQT1JUIiwic2V0Iiwic2VydmVyIiwiY3JlYXRlU2VydmVyIiwibGlzdGVuIiwib24iLCJvbkVycm9yIiwib25MaXN0ZW5pbmciLCJ2YWwiLCJwYXJzZUludCIsImlzTmFOIiwiZXJyb3IiLCJzeXNjYWxsIiwiYmluZCIsImNvZGUiLCJjb25zb2xlIiwiZXhpdCIsImFkZHIiLCJhZGRyZXNzIl0sIm1hcHBpbmdzIjoiOztBQUVBOzs7O0FBSUEsSUFBSUEsTUFBTUMsUUFBUSxRQUFSLENBQVY7QUFDQSxJQUFJQyxRQUFRRCxRQUFRLE9BQVIsRUFBaUIsZUFBakIsQ0FBWjtBQUNBLElBQUlFLE9BQU9GLFFBQVEsTUFBUixDQUFYOztBQUVBOzs7O0FBSUEsSUFBSUcsT0FBT0MsY0FBY0MsUUFBUUMsR0FBUixDQUFZQyxJQUFaLElBQW9CLE1BQWxDLENBQVg7QUFDQVIsSUFBSVMsR0FBSixDQUFRLE1BQVIsRUFBZ0JMLElBQWhCOztBQUVBOzs7O0FBSUEsSUFBSU0sU0FBU1AsS0FBS1EsWUFBTCxDQUFrQlgsR0FBbEIsQ0FBYjs7QUFFQTs7OztBQUlBVSxPQUFPRSxNQUFQLENBQWNSLElBQWQ7QUFDQU0sT0FBT0csRUFBUCxDQUFVLE9BQVYsRUFBbUJDLE9BQW5CO0FBQ0FKLE9BQU9HLEVBQVAsQ0FBVSxXQUFWLEVBQXVCRSxXQUF2Qjs7QUFFQTs7OztBQUlBLFNBQVNWLGFBQVQsQ0FBdUJXLEdBQXZCLEVBQTRCO0FBQzFCLE1BQUlaLE9BQU9hLFNBQVNELEdBQVQsRUFBYyxFQUFkLENBQVg7O0FBRUEsTUFBSUUsTUFBTWQsSUFBTixDQUFKLEVBQWlCO0FBQ2Y7QUFDQSxXQUFPWSxHQUFQO0FBQ0Q7O0FBRUQsTUFBSVosUUFBUSxDQUFaLEVBQWU7QUFDYjtBQUNBLFdBQU9BLElBQVA7QUFDRDs7QUFFRCxTQUFPLEtBQVA7QUFDRDs7QUFFRDs7OztBQUlBLFNBQVNVLE9BQVQsQ0FBaUJLLEtBQWpCLEVBQXdCO0FBQ3RCLE1BQUlBLE1BQU1DLE9BQU4sS0FBa0IsUUFBdEIsRUFBZ0M7QUFDOUIsVUFBTUQsS0FBTjtBQUNEOztBQUVELE1BQUlFLE9BQU8sT0FBT2pCLElBQVAsS0FBZ0IsUUFBaEI7QUFDUCxZQUFVQSxJQURIO0FBRVAsWUFBVUEsSUFGZDs7QUFJQTtBQUNBLFVBQVFlLE1BQU1HLElBQWQ7QUFDRSxTQUFLLFFBQUw7QUFDRUMsY0FBUUosS0FBUixDQUFjRSxPQUFPLCtCQUFyQjtBQUNBZixjQUFRa0IsSUFBUixDQUFhLENBQWI7QUFDQTtBQUNGLFNBQUssWUFBTDtBQUNFRCxjQUFRSixLQUFSLENBQWNFLE9BQU8sb0JBQXJCO0FBQ0FmLGNBQVFrQixJQUFSLENBQWEsQ0FBYjtBQUNBO0FBQ0Y7QUFDRSxZQUFNTCxLQUFOLENBVko7O0FBWUQ7O0FBRUQ7Ozs7QUFJQSxTQUFTSixXQUFULEdBQXVCO0FBQ3JCLE1BQUlVLE9BQU9mLE9BQU9nQixPQUFQLEVBQVg7QUFDQSxNQUFJTCxPQUFPLE9BQU9JLElBQVAsS0FBZ0IsUUFBaEI7QUFDUCxZQUFVQSxJQURIO0FBRVAsWUFBVUEsS0FBS3JCLElBRm5CO0FBR0FGLFFBQU0sa0JBQWtCbUIsSUFBeEI7QUFDRCIsImZpbGUiOiJ3d3cuanMiLCJzb3VyY2VzQ29udGVudCI6WyJcblxuLyoqXG4gKiBNb2R1bGUgZGVwZW5kZW5jaWVzLlxuICovXG5cbnZhciBhcHAgPSByZXF1aXJlKCcuLi9hcHAnKTtcbnZhciBkZWJ1ZyA9IHJlcXVpcmUoJ2RlYnVnJykoJ2FwaS13czpzZXJ2ZXInKTtcbnZhciBodHRwID0gcmVxdWlyZSgnaHR0cCcpO1xuXG4vKipcbiAqIEdldCBwb3J0IGZyb20gZW52aXJvbm1lbnQgYW5kIHN0b3JlIGluIEV4cHJlc3MuXG4gKi9cblxudmFyIHBvcnQgPSBub3JtYWxpemVQb3J0KHByb2Nlc3MuZW52LlBPUlQgfHwgJzMwMDAnKTtcbmFwcC5zZXQoJ3BvcnQnLCBwb3J0KTtcblxuLyoqXG4gKiBDcmVhdGUgSFRUUCBzZXJ2ZXIuXG4gKi9cblxudmFyIHNlcnZlciA9IGh0dHAuY3JlYXRlU2VydmVyKGFwcCk7XG5cbi8qKlxuICogTGlzdGVuIG9uIHByb3ZpZGVkIHBvcnQsIG9uIGFsbCBuZXR3b3JrIGludGVyZmFjZXMuXG4gKi9cblxuc2VydmVyLmxpc3Rlbihwb3J0KTtcbnNlcnZlci5vbignZXJyb3InLCBvbkVycm9yKTtcbnNlcnZlci5vbignbGlzdGVuaW5nJywgb25MaXN0ZW5pbmcpO1xuXG4vKipcbiAqIE5vcm1hbGl6ZSBhIHBvcnQgaW50byBhIG51bWJlciwgc3RyaW5nLCBvciBmYWxzZS5cbiAqL1xuXG5mdW5jdGlvbiBub3JtYWxpemVQb3J0KHZhbCkge1xuICB2YXIgcG9ydCA9IHBhcnNlSW50KHZhbCwgMTApO1xuXG4gIGlmIChpc05hTihwb3J0KSkge1xuICAgIC8vIG5hbWVkIHBpcGVcbiAgICByZXR1cm4gdmFsO1xuICB9XG5cbiAgaWYgKHBvcnQgPj0gMCkge1xuICAgIC8vIHBvcnQgbnVtYmVyXG4gICAgcmV0dXJuIHBvcnQ7XG4gIH1cblxuICByZXR1cm4gZmFsc2U7XG59XG5cbi8qKlxuICogRXZlbnQgbGlzdGVuZXIgZm9yIEhUVFAgc2VydmVyIFwiZXJyb3JcIiBldmVudC5cbiAqL1xuXG5mdW5jdGlvbiBvbkVycm9yKGVycm9yKSB7XG4gIGlmIChlcnJvci5zeXNjYWxsICE9PSAnbGlzdGVuJykge1xuICAgIHRocm93IGVycm9yO1xuICB9XG5cbiAgdmFyIGJpbmQgPSB0eXBlb2YgcG9ydCA9PT0gJ3N0cmluZydcbiAgICA/ICdQaXBlICcgKyBwb3J0XG4gICAgOiAnUG9ydCAnICsgcG9ydDtcblxuICAvLyBoYW5kbGUgc3BlY2lmaWMgbGlzdGVuIGVycm9ycyB3aXRoIGZyaWVuZGx5IG1lc3NhZ2VzXG4gIHN3aXRjaCAoZXJyb3IuY29kZSkge1xuICAgIGNhc2UgJ0VBQ0NFUyc6XG4gICAgICBjb25zb2xlLmVycm9yKGJpbmQgKyAnIHJlcXVpcmVzIGVsZXZhdGVkIHByaXZpbGVnZXMnKTtcbiAgICAgIHByb2Nlc3MuZXhpdCgxKTtcbiAgICAgIGJyZWFrO1xuICAgIGNhc2UgJ0VBRERSSU5VU0UnOlxuICAgICAgY29uc29sZS5lcnJvcihiaW5kICsgJyBpcyBhbHJlYWR5IGluIHVzZScpO1xuICAgICAgcHJvY2Vzcy5leGl0KDEpO1xuICAgICAgYnJlYWs7XG4gICAgZGVmYXVsdDpcbiAgICAgIHRocm93IGVycm9yO1xuICB9XG59XG5cbi8qKlxuICogRXZlbnQgbGlzdGVuZXIgZm9yIEhUVFAgc2VydmVyIFwibGlzdGVuaW5nXCIgZXZlbnQuXG4gKi9cblxuZnVuY3Rpb24gb25MaXN0ZW5pbmcoKSB7XG4gIHZhciBhZGRyID0gc2VydmVyLmFkZHJlc3MoKTtcbiAgdmFyIGJpbmQgPSB0eXBlb2YgYWRkciA9PT0gJ3N0cmluZydcbiAgICA/ICdwaXBlICcgKyBhZGRyXG4gICAgOiAncG9ydCAnICsgYWRkci5wb3J0O1xuICBkZWJ1ZygnTGlzdGVuaW5nIG9uICcgKyBiaW5kKTtcbn1cbiJdfQ==